Test issue described at
https://github.com/ivelum/job/blob/master/code_challenges/python.md

Code can be much more compact you know, but it should be like that
at production environment. I just want to demonstrate my vision of
flexibility & maintain.

*********************************
Installation
*********************************
This projects uses python 3 so it should be installed.
Server running at 8000 port by default.

Start the project using docker:

::

  docker build . -t habraproxy_fu2re
  docker run -it -p 8000:8000 habraproxy_fu2re


*********************************
Testing
*********************************

::

  docker build . -t habraproxy_fu2re
  docker run -it habraproxy_fu2re python manage.py test

*********************************
Known issues
*********************************

- Some advertising can work incorrectly due to obvious reasons. Host is not verified or something like that.
- POST disabled to prevent leaving the proxy-site.