FROM python:3.6
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt
RUN pip install gunicorn

COPY . /code/
WORKDIR /code/

EXPOSE 8000
CMD exec gunicorn test_issue.wsgi:application --bind 0.0.0.0:8000 --workers 3