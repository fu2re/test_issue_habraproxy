import re
import os
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from unittest import mock
from pyfakefs.fake_filesystem_unittest import TestCase as FakeFsTestCase
from django.test import SimpleTestCase
from django.test import Client
from django.conf import settings
from .views import SignProcessor
from .utils import download_file


class UtilsTestCase(FakeFsTestCase):
    URL_EXISTING_AT_ORIGIN = '/images/habr.png'

    def setUp(self):
        self.setUpPyfakefs(
            additional_skip_names=['requests']
        )

    def test_downloader(self):
        url = urljoin(settings.HABRA_URL, self.URL_EXISTING_AT_ORIGIN)
        download_file(url, settings.MEDIA_ROOT)
        path = os.path.join(settings.MEDIA_ROOT, self.URL_EXISTING_AT_ORIGIN.replace('/', os.sep).lstrip('/'))
        self.assertTrue(os.path.exists(path))

    def test_downloader_does_not_exists(self):
        url = urljoin(settings.HABRA_URL, 'blabla.jpg')
        download_file(url, settings.MEDIA_ROOT)
        path = os.path.join(settings.MEDIA_ROOT, self.URL_EXISTING_AT_ORIGIN.replace('/', os.sep).lstrip('/'))
        self.assertFalse(os.path.exists(path))


class SignProcessorTestCase(SimpleTestCase):
    def setUp(self):
        self.lorem = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in 
        culpa qui officia deserunt mollit anim id est laborum.""".replace('\n', '')
    
    def assertSignedContent(self, processor, value=1):
        body = processor.soup.html.body
        self.assertEqual(body.prettify().count(settings.HABRA_SIGN), value)
    
    @staticmethod
    def __sign__(content, *args, **kwargs):
        return SignProcessor(
            '<html><head></head><body>{}</body></html>'.format(content),
            *args, **kwargs
        )

    def test_rule_init(self):
        sign = self.__sign__('')
        sign.get_content()
        self.assertEqual(type(sign.soup), BeautifulSoup)

    def test_remove_comments(self):
        processor = self.__sign__(' <!--Test--> ')
        processor.remove_comments()
        self.assertEqual(processor.soup.html.body.encode_contents().strip(), b'')

    def test_text(self):
        sentence = self.lorem
        processor = self.__sign__(sentence)
        processor.sign()
        self.assertSignedContent(processor, 9)

    def test_empty(self):
        sentence = self.lorem
        # cut off all matched words.
        sentence = re.sub('([^\w\s]|\s(\w{%s})[\s\W])' % settings.HABRA_TARGET_LENGTH, '', sentence, flags=re.UNICODE)
        processor = self.__sign__(sentence)
        processor.sign()
        self.assertSignedContent(processor, 0)

    def test_html_symbols(self):
        processor = self.__sign__('&curren; &brvbar; &yen; &laquo; &plus;1 ')
        processor.sign()
        self.assertSignedContent(processor, 0)

    def __test_rule__(self, sentence, count=1):
        processor = self.__sign__(sentence)
        processor.sign()
        self.assertSignedContent(processor, count)

    def test_seq(self):
        self.__test_rule__("google google", 2)

    def test_rule_colon(self):
        self.__test_rule__("google:")

    def test_rule_space(self):
        self.__test_rule__(" google ")

    def test_rule_start(self):
        self.__test_rule__("google ")

    def test_rule_end(self):
        self.__test_rule__(" google")

    def test_rule_comma(self):
        self.__test_rule__("google,")

    def test_rule_dot(self):
        self.__test_rule__("google.")

    def test_rule_semicolon(self):
        self.__test_rule__("google;")

    def test_rule_dots(self):
        self.__test_rule__("google...")

    def test_rule_question(self):
        self.__test_rule__("google?")

    def test_rule_super_question(self):
        self.__test_rule__("google???")

    def test_rule_exclamation(self):
        self.__test_rule__("google!")

    def test_rule_super_exclamation(self):
        self.__test_rule__("google!!!")

    def test_rule_question_exclamation(self):
        self.__test_rule__("google?!")

    def test_rule_bracket(self):
        self.__test_rule__("(google)")

    def test_rule_brace(self):
        self.__test_rule__("{google}")

    def test_rule_squared_bracket(self):
        self.__test_rule__("[google]")

    def test_rule_apostrophe(self):
        self.__test_rule__("'google'")

    def test_rule_quotation_mark(self):
        self.__test_rule__("\"google\"")

    def test_rule_double_quote_2(self):
        self.__test_rule__("\"черное яблоко\"", 2)

    def test_rule_slash(self):
        self.__test_rule__("Erlang/OTP")

    def test_rule_slash_error(self):
        self.__test_rule__("Erlang//OTP", 0)

    def test_rule_single_quote(self):
        self.__test_rule__("\u2018google\u2019")

    def test_rule_double_quote(self):
        self.__test_rule__("\u201Cgoogle\u201D")

    def test_rule_classic_quote(self):
        self.__test_rule__("\u00ABgoogle\u00BB")

    def test_rule_classic_quote_html(self):
        self.__test_rule__("&laquo;google&raquo;")

    def test_downloader_replacements(self):
        url = UtilsTestCase.URL_EXISTING_AT_ORIGIN
        processor = self.__sign__('<use xlink:href="{}"></a>'.format(
            urljoin(settings.HABRA_URL, url)
        ))
        with mock.patch.object(os.path, 'exists', new=lambda x: True):
            processor.file_downloader()
        self.assertEqual(processor.soup.html.use['xlink:href'], urljoin(settings.MEDIA_URL, url.lstrip('/')))

    def test_rewrite_url(self):
        processor = self.__sign__('<a href="{}"></a>'.format(settings.HABRA_URL))
        processor.rewrite_url()
        self.assertEqual(processor.soup.html.a['href'], '')

    def test_rewrite_url_mirror(self):
        mirror = 'http://habrahabr.ru'
        path = '/ru/'
        processor = self.__sign__('<a href="{}"></a>'.format(urljoin(mirror, path)), mirrors=mirror)
        processor.rewrite_url()
        self.assertEqual(processor.soup.html.a['href'], path)

    def test_rewrite_url_google(self):
        url = 'http://google.com'
        processor = self.__sign__('<a href="{}"></a>'.format(url))
        processor.rewrite_url()
        self.assertEqual(processor.soup.html.a['href'], url)

    def test_pipeline(self):
        sentence = 'Google!'
        processor = self.__sign__('<a href="{}"><!--COMMENT-->{}</a>'.format(settings.HABRA_URL, sentence))
        processor.get_content()
        self.assertSignedContent(processor)
        self.assertEqual(processor.soup.html.a['href'], '')
        self.assertEqual(processor.soup.html.a.text[:-2], sentence[:-1])


class HabraSignTestCase(SimpleTestCase):
    def test_view(self):
        client = Client()
        with mock.patch.object(os.path, 'exists', new=lambda x: True):
            response = client.get('')
        self.assertEqual(response.status_code, 200)

    def test_view_404(self):
        client = Client()
        response = client.get('/ru/404/')
        self.assertEqual(response.status_code, 404)
