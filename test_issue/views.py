import re
import requests
from urllib.parse import urljoin
from requests.exceptions import ConnectionError
import html.parser as hp
from bs4 import BeautifulSoup
from bs4.element import Comment
from django.conf import settings
from django.views.generic.base import ContextMixin, View
from django.http import HttpResponse, HttpResponseServerError, HttpResponseRedirect
from .utils import download_file


class SignProcessor(object):
    pipeline = [
        'remove_comments',
        'file_downloader',
        'sign',
        'rewrite_url',
    ]

    def __init__(self, content, exclude_tags=None, mirrors=None):
        """

        :param content: origin content
        :param exclude_tags: tag names which will not being processed
        :param mirrors: regex string contains origin website urls
        :type content: bytes
        :type exclude_tags: list
        :type mirrors: str
        """
        self.soup = BeautifulSoup(content, "html5lib")
        self.exclude_tags = ['script', 'style', 'head', 'meta'] + (exclude_tags or ['code'])
        mirrors = mirrors or 'http(s){0,1}://habrahabr\.ru?'
        self.mirrors = re.compile(
            '^({})'.format('|'.join((re.escape(settings.HABRA_URL), mirrors))), flags=re.UNICODE)

    def sign(self):
        """
        Transform text
        """
        target_length = settings.HABRA_TARGET_LENGTH
        # brackets, brace, quotes, etc.
        re_quotes_opened = '[({\\[\u0022\u0027\u00AB\u201C\u2018]{1}'
        re_quotes_closed = '[)}\\]\u0022\u0027\u00BB\u201D\u2019]{1}'
        re_attached_signs = '|'.join([
            '\?!',  # exclamation + question
            '\?{1,3}',  # question
            '!{1,3}',  # exclamation
            '\.{1,3}',  # dots
            '[:;,]{1}',  # comma, semicolon, colon
            '/\w'  # slash is the only possible character inside a word
        ])
        html_symbol = '(&[a-zA-Z];?)'
        # make the formula more readable
        rule_formula = '(?P<start>\A|\s|{html_symbol}|{re_quotes_opened})' + \
                       '(?P<word>\w{{{target_length}}})' + \
                       '(?={re_quotes_closed}|{html_symbol}|{re_attached_signs}|\s|$)'
        rule = rule_formula.format(**locals())
        pattern = re.compile(rule, flags=re.UNICODE)
        output = r'\g<start>\g<word>{}\g<2>'.format(settings.HABRA_SIGN)
        for tag in self.soup.find_all(text=re.compile('\w+')):
            if tag.parent.name in self.exclude_tags:
                continue
            tag.string.replace_with(
                pattern.sub(output, tag.string)
            )

    def rewrite_url(self):
        """
        Transform links
        """
        rewrite_rules = {
            'a': 'href',
            'form': 'action'
        }

        links = self.soup.find_all(lambda x: rewrite_rules.get(x.name) and x.get(rewrite_rules[x.name]))
        for tag in links:
            tag.attrs[rewrite_rules[tag.name]] = \
                self.mirrors.sub('', tag.attrs[rewrite_rules[tag.name]])

    def file_downloader(self):
        """
        Download svg files.
        """
        download_rules = {
            'use': 'xlink:href'
        }
        tags = self.soup.find_all(lambda x: download_rules.get(x.name) and x.get(download_rules[x.name]))
        for tag in tags:
            remote_url = tag.attrs[download_rules[tag.name]]
            tag.attrs[download_rules[tag.name]] = self.mirrors.sub(settings.MEDIA_URL.rstrip('/'), remote_url)
            download_file(remote_url)

    def remove_comments(self):
        """
        Remove html commentaries
        """
        comments = self.soup.find_all(text=lambda text: isinstance(text, Comment))
        for comment in comments:
            comment.extract()

    def get_content(self):
        """
        Perform a content transformation through the pipeline.

        :return: fully processed origin content
        :rtype: str
        """
        for action in self.pipeline:
            getattr(self, action)()
        return self.soup.html.prettify(formatter='html')


class HabraSign(ContextMixin, View):
    @staticmethod
    def __get_source__(path, request):
        """
        Get content from an origin site

        :param path: relative path at origin site
        :param request:
        :type path: str
        :type request: Django HttpRequest object
        :return: response with content from an origin site
        :rtype: requests library Response object
        """
        headers = {'User-Agent': request.META.get('HTTP_USER_AGENT')}
        try:
            return requests.get(urljoin(settings.HABRA_URL, path), headers=headers)
        except ConnectionError:
            return None

    def get(self, request, path, *args, **kwargs):
        if re.search('(\.js|css|jpg|png)$', path):
            return HttpResponseRedirect(urljoin(settings.HABRA_URL, path))
        source = self.__get_source__(path, request)
        if source is None:
            return HttpResponseServerError('Something went wrong. Try again later.')

        return HttpResponse(
            SignProcessor(source.content).get_content() if
            source.headers.get('content-type').startswith('text/html') else
            source.content, status=source.status_code)

    def post(self, request, path, *args, **kwargs):
        return HttpResponseServerError('POST is not supported')
