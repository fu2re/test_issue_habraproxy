import re
import os
import requests
from requests.exceptions import ConnectionError
from urllib.parse import urlparse
from django.conf import settings


def download_file(url, save_root=settings.MEDIA_ROOT):
    """
    Download file from remote server. Saves the file with the same
    folder structure as remote server have.

    :param url: remote url
    :param save_root: root folder
    :type url: str
    :type save_root: str

    :return: True if new file was created
    :rtype bool:
    """
    url_path = re.sub('#.+$', '', urlparse(url).path)
    save_path = os.path.join(save_root, url_path.lstrip('/'))
    if not os.path.exists(save_path):
        try:
            os.makedirs(os.path.dirname(save_path), exist_ok=True)
            r = requests.get(url, allow_redirects=True)
            if r.status_code == 200:
                open(save_path, 'wb').write(r.content)
        except ConnectionError:
            pass
